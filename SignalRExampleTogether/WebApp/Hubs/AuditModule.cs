﻿using System;
using Microsoft.AspNet.SignalR.Hubs;

namespace WebApp.Hubs
{
    public class AuditModule : HubPipelineModule
    {
        protected override void OnIncomingError(ExceptionContext exceptionContext, IHubIncomingInvokerContext invokerContext)
        {
            Console.WriteLine($"Error {exceptionContext.Error.Message}");
            base.OnIncomingError(exceptionContext, invokerContext);
        }
    }
}