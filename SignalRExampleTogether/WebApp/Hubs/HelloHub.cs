﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;

namespace WebApp.Hubs
{
    public class HelloHub : Hub
    {
        public override Task OnDisconnected(bool stopCalled)
        {
            Console.WriteLine($"Disconnected {Context.ConnectionId} {Context.User.Identity.Name}");
            return base.OnDisconnected(stopCalled);
        }
    }
}