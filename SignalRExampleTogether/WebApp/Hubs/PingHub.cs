﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using WebApp.Controllers;

namespace WebApp.Hubs
{
    public class PingHub : Hub
    {
        private readonly IMessageProvider _provider;

        public PingHub(IMessageProvider provider)
        {
            _provider = provider;
        }

        public void Ping()
        {
            string message = _provider.BuildMessage(Context.User.Identity.Name);
            Clients.Caller.Ping(message);
            Clients.All.Ping($"From {Context.ConnectionId}");
            Clients.User("pippo@example.com").Ping("Direct ping to pippo");

            Clients.Caller.SendData("Pippo", 19);
            Clients.Caller.SendTypedData(new { Text = "Pippo", Value = 19 });
        }

        [Authorize(Users = "pippo@example.com")]
        public void RestrictedPing()
        {
            Clients.Caller.Ping("Ok!");
        }

        public override Task OnConnected()
        {
            Console.WriteLine($"Connected {Context.ConnectionId}");
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            Console.WriteLine($"Disconnected {Context.ConnectionId} {Context.User.Identity.Name}");
            return base.OnDisconnected(stopCalled);
        }

        public override Task OnReconnected()
        {
            Console.WriteLine($"Reconnected {Context.ConnectionId}");
            return base.OnReconnected();
        }
    }
}