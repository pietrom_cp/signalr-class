﻿using System.Web.Mvc;
using Microsoft.AspNet.SignalR;

namespace WebApp.Controllers
{
    public class HelloController : Controller
    {
        private readonly IMessageProvider _provider;
        private readonly IHubContext _hubContext;

        public HelloController(IMessageProvider provider, IHubContext hubContext)
        {
            _provider = provider;
            _hubContext = hubContext;
        }

        public ActionResult Hello(string to)
        {
            string message = BuildResponseMessage(to);
            _hubContext.Clients.All.SendMessage(message);
            return Json(new { Message = message }, JsonRequestBehavior.AllowGet);
        }

        private string BuildResponseMessage(string to)
        {
            return _provider.BuildMessage(to);
        }
    }
}