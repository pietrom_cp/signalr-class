﻿namespace WebApp.Controllers
{
    public class BasicMessageProvider : IMessageProvider
    {
        public string BuildMessage(string to)
        {
            return $"Hello, {to}!";
        }
    }
}