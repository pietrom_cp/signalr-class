﻿using Castle.MicroKernel.Registration;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.AspNet.SignalR.Infrastructure;
using Microsoft.Owin;
using Owin;
using SignalR.Castle.Windsor;
using WebApp.App_Start;
using WebApp.Hubs;

[assembly: OwinStartupAttribute(typeof(WebApp.Startup))]
namespace WebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var boostrapper = ContainerBootstrapper.Bootstrap();
            ConfigureAuth(app);

            IDependencyResolver resolver = new WindsorDependencyResolver(boostrapper.Container);
            var context = resolver.Resolve<IConnectionManager>().GetHubContext<HelloHub>();
            var pipeline = resolver.Resolve<IHubPipeline>();
            var modules = boostrapper.Container.ResolveAll<IHubPipelineModule>();
            foreach (IHubPipelineModule module in modules)
            {
                pipeline.AddModule(module);
            }

            boostrapper.Container.Register(Component.For<IHubContext>().
                Instance(context).LifestyleSingleton());

            app.MapSignalR("/signalr", new HubConfiguration
            {
                Resolver = resolver
            });
        }
    }
}
