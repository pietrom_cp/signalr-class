﻿function testSignalrConnection() {
    //$.connection.hub.logging = true;
    //$.connection.hub.log = console.log;
    var pingHub = $.connection.pingHub;

    pingHub.client.ping = function (msg) {
        console.log("Server ping received:", msg);
    };

    pingHub.client.sendData = function (msg, n) {
        console.log("Server ping received data:", msg, n);
    };

    pingHub.client.sendTypedData = function (data) {
        console.log("Server ping received data:", data, data.Text, data.Value);
    };

    var helloHub = $.connection.helloHub;
    helloHub.client.sendMessage = function(message) {
        console.log('Received message from HelloController', message);
    };

    $.connection.hub.error(console.log);


    $.connection.hub.start({ }).done(function () {
        console.log($.connection.hub.transport.name);
        pingHub.server.ping();
        pingHub.server.restrictedPing().fail(function(err) { console.log('FAILED', err); });
    });
}

$(testSignalrConnection);

function sayHelloTo(target) {
    var url = "/Hello/Hello";
    $.ajax(url + '?to=' + target, {
        success: function onSuccess(data) {
            console.log('Server response:', JSON.stringify(data));
        }, dataType: "json"
    });
}

function stopSignalr() {
    $.connection.hub.stop();
}