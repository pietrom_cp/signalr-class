﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR.Client;

namespace ConsoleApp
{
    class Data
    {
        public string Text { get; set; }
        public int Value { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Execute().ContinueWith(t =>
            {
                Console.WriteLine("Completed!");
                if (t.IsFaulted)
                {
                    Console.WriteLine(t.Exception.Message);
                }
            });
            Console.ReadKey();
        }

        static async Task Execute()
        {
            HubConnection conn = new HubConnection("http://localhost:64408/signalr");
            IHubProxy proxy = conn.CreateHubProxy("PingHub");
            proxy.On<string>("Ping", text =>
            {
                Console.WriteLine($"Server sent message: {text}");
            });
            proxy.On<string, int>("SendData", (txt, n) =>
            {
                Console.WriteLine($"Server sent message: {txt} / {n}");
            });
            proxy.On<Data>("SendTypedData", (data) =>
            {
                Console.WriteLine($"Server sent typed data: {data.Text} / {data.Value}");
            });
            await conn.Start();
            await proxy.Invoke("Ping");
        }
    }
}
