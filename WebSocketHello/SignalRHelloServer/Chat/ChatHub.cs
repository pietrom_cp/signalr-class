﻿using Huxley.Net.Common;
using Microsoft.AspNet.SignalR;
using System;
using System.Threading.Tasks;

namespace SignalRHelloServer.Chat
{
    public class ChatHub : Hub
    {
        public override Task OnConnected() {
            var transport = Context.QueryString["transport"];
            return base.OnConnected();
        }
        public void Send(string name, string message) {
            string from = name.IsNullOrWhiteSpace() ? Context.User.Identity.Name : name;
            Clients.All.BroadcastMessage(from, message);
            Clients.User("pie00").LogMessage(from, message);
        }

        public async Task DoLongRunningThing()
        {
            for (int i = 0; i < 100; i++)
            {
                await Task.Delay(200);
                Clients.Caller.UpdateProgress(i);
            }
            Clients.Caller.JobCompleted("Something expensive");
        }
        [Authorize(Roles = "Role0,Role2")]
        public Task JoinRoom(string roomName)
        {
            return Groups.Add(Context.ConnectionId, roomName);
        }

        public void Send(string name, string message, string roomName)
        {
            string from = name.IsNullOrWhiteSpace() ? Context.User.Identity.Name : name;
            Clients.Group(roomName).BroadcastToRoom(roomName, from, message);
        }
    }
}
