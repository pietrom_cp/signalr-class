﻿using Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.StaticFiles;
using Microsoft.Owin.FileSystems;
using System.Web.Http;
using Microsoft.AspNet.SignalR;
using SignalRHelloServer.Persistent;
using Microsoft.Owin.Security.Cookies;
using System.Security.Claims;
using System.IO;
using System.Net;
using System.Collections.Generic;
using Microsoft.AspNet.SignalR.Hubs;

namespace ServerSentEventsHelloServer
{
    public class Startup
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            HttpConfiguration config = new HttpConfiguration();
            config.MapHttpAttributeRoutes();
            appBuilder.UseCors(CorsOptions.AllowAll);
            config.EnsureInitialized();

            appBuilder.UseWebApi(config);
            const string contentPath = @"C:\work\corsi\signalr\source\WebSocketHello\SignalRHelloServer\Scripts";
            var physicalFileSystem = new PhysicalFileSystem(contentPath);
            var options = new FileServerOptions
            {
                EnableDefaultFiles = true,
                FileSystem = physicalFileSystem
            };
            options.StaticFileOptions.FileSystem = physicalFileSystem;
            options.StaticFileOptions.ServeUnknownFileTypes = true;
            options.DefaultFilesOptions.DefaultFileNames = new[]
            {
                "index.html"
            };

            var authOptions = new CookieAuthenticationOptions()
            {
                AuthenticationType = CookieAuthenticationDefaults.AuthenticationType,
                LoginPath = new Microsoft.Owin.PathString("/login-form.htm"),
                LogoutPath = CookieAuthenticationDefaults.LogoutPath,
            };

            appBuilder.UseCookieAuthentication(authOptions);

            var loginForm = File.ReadAllBytes(Path.Combine(contentPath, @"login-form.html"));

            appBuilder.Use(async (context, next) =>
            {
                var redirectUri = context.Request.Query["ReturnUrl"];

                if (context.Request.Path.Value.Contains(authOptions.LoginPath.Value))
                {
                    if (context.Request.Method == "POST")
                    {
                        var form = await context.Request.ReadFormAsync();
                        var userName = form["username"];
                        var password = form["password"];

                        if (!ValidateUserCredentials(userName, password))
                        {
                            var redirect = authOptions.LoginPath.Value;
                            if (!string.IsNullOrEmpty(redirectUri))
                            {
                                redirect += "?ReturnUrl=" + WebUtility.UrlEncode(redirectUri);
                            }

                            context.Response.Redirect(redirect);
                        }

                        var identity = new ClaimsIdentity(authOptions.AuthenticationType);
                        identity.AddClaim(new Claim(ClaimTypes.Name, userName));
                        foreach (string role in Roles[userName].Split(',')) {
                            identity.AddClaim(new Claim(ClaimTypes.Role, role));
                        }                        
                        context.Authentication.SignIn(identity);

                        redirectUri = redirectUri ?? "/index.html";
                        context.Response.Redirect(redirectUri);
                    }
                    else
                    {
                        context.Response.ContentType = "text/html";
                        await context.Response.WriteAsync(loginForm);
                    }
                }
                else if (context.Request.Path.Value.Contains(authOptions.LogoutPath.Value))
                {
                    context.Authentication.SignOut(authOptions.AuthenticationType);
                    redirectUri = redirectUri ?? authOptions.LoginPath.Value;
                    context.Response.Redirect(redirectUri);
                }
                else if (context.Request.User == null || !context.Request.User.Identity.IsAuthenticated)
                {
                    context.Response.Redirect(authOptions.LoginPath.Value);
                }
                else if (context.Request.Path.Value == "/")
                {
                    context.Response.Redirect("/index.html");
                }
                else
                {
                    await next();
                }
            });

            appBuilder.UseFileServer(options);

            var hubConfiguration = new HubConfiguration { Resolver = new DefaultDependencyResolver() };
           // hubConfiguration.Resolver.UseRedis("localhost", 6379, "YourStrongPassword1234", "SignalRClass");
           //shubConfiguration.Resolver.UseSqlServer("server=localhost;database=MySample;user id=sample;password=s4mplep4ss");
            //hubConfiguration.Resolver.Resolve<IHubPipeline>().AddModule(new SampleModule());
            appBuilder.MapSignalR<ChatConnection>("/real-time/chat", hubConfiguration);
            appBuilder.MapSignalR("/real-time", hubConfiguration);            
        }

        private bool ValidateUserCredentials(string username, string password)
        {
            return username == password && Roles.ContainsKey(username);
        }

        private static readonly IDictionary<string, string> Roles = new Dictionary<string, string> {
            { "pie00", "Role0,Role1" },
            { "pie01", "Role1,Role2" },
            { "pie02", "Role2,Role3" },
            { "pie03", "Role3,Role4" },
            { "pie04", "Role4,Role5" }
        };
    }

    internal class SampleModule : HubPipelineModule {
        protected override void OnIncomingError(ExceptionContext exceptionContext, IHubIncomingInvokerContext invokerContext) {
            base.OnIncomingError(exceptionContext, invokerContext);
        }
    }
}
