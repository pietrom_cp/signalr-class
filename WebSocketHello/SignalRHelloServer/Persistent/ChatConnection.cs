﻿using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;

namespace SignalRHelloServer.Persistent
{
    public class ChatConnection : PersistentConnection
    {
        protected override async Task OnReceived(IRequest request, string connectionId, string data)
        {
            await Connection.Broadcast(data);
            await Connection.Send(connectionId, new { Data = data, Status = "Ok" });
        }
    }
}
