﻿using System;
using System.Collections.Generic;
using System.Web.Http;

namespace ServerSentEventsHelloServer.Events
{
    [RoutePrefix("events/synch")]
    public class SynchronousPollingController : ApiController
    {
        private static readonly Random Random = new Random();
        [Route("")]
        public IEnumerable<string> GetEvents()
        {
            IList<string> events = new List<string>();
            int count = Random.Next(6);
            for (int i = 0; i < count; i++)
            {
                events.Add($"Event {i} at {DateTimeOffset.Now}");
            }
            return events;
        }
    }
}
