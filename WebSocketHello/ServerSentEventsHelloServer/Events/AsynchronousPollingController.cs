﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace ServerSentEventsHelloServer.Events
{
    [RoutePrefix("events/asynch")]
    public class AsynchronousPollingController : ApiController
    {
        private static readonly Random Random = new Random();
        [Route("")]
        public async Task<IEnumerable<string>> GetEvents()
        {
            //IList<string> events = new List<string>();
            //int count = Random.Next(6);
            //for (int i = 0; i < count; i++)
            //{
            //    events.Add($"Event {i} at {DateTimeOffset.Now}");
            //}
            //int sleepForSeconds = Random.Next(5);
            //await Task.Delay(sleepForSeconds * 1000);
            //return events;
            return EventContainer.GetEvents(TimeSpan.FromSeconds(5));
        }
    }
}
