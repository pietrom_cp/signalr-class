﻿using Microsoft.Owin.Hosting;
using System;
using ServerSentEventsHelloServer.Events;

namespace ServerSentEventsHelloServer
{
    class Program
    {
        static void Main()
        {
            string baseAddress = "http://localhost:3456/";
            var host = WebApp.Start<Startup>(url: baseAddress);
            Console.WriteLine("WebApi started. Press any key to exit");
            string input = null;
            bool go = true;
            while (go) {
                Console.WriteLine("Please insert event text (exit to exit):");
                input = Console.ReadLine();
                if (input == "exit") {
                    go = false;
                } else {
                    EventContainer.Enqueue(input);
                }
            }
            Console.ReadKey();
        }
    }
}
